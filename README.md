# Deno Delight

A very small web framework inspired by laravel/lumen

## The project's Vision

Deno Delight aims to offer a simple yet powerful web starting ground for
deno-based projects. Inspired by other NodeJS Frameworks, it should take the
better and drop the worst.

## Features

Here is a small list of some features which Deno Delight is composed of:

 - [ ] Environment (`.env` files and OS environment variables)
 - [ ] Middleware
 - [ ] Routing
 - [ ] Controllers
 - [ ] Simple ORM (including Models)